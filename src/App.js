import React, {
  useState,
} from 'react';
import './App.css';

export default function App() {
  const [ result, setResult ] = useState('0');
  const [ sign, setSign ] = useState('');
  const [ number, setNumber ] = useState('0');
  const [ checkInput, setCheckInput ] = useState(true);

  const reset = () => {
    setResult('0');
    setSign('');
    setNumber('0');
    setCheckInput(true);
  };

  const formatNumber = (val) => {
    return val.toString().substr(0, 11);
  }

  const handlePercent = () => {
    if (sign === '') return;

    const r = parseFloat(result);
    let n = parseFloat(number);

    n = r * n / 100;

    setNumber(formatNumber(n));
  };

  const handleEqual = () => {
      let r = parseFloat(result);
      const n = parseFloat(number);

      switch(sign) {
        case 'x':
          r *= n; break;
        case '÷':
          r /= n; break;
        case '+':
          r += n; break;
        case '—':
          r -= n; break;
        default:
          r = n; break;
      }

    setNumber(formatNumber(r));
    setResult(formatNumber(r));
    setSign('');
    setCheckInput(false);
  };

  const handleSign = (val) => {
    if (sign !== '') {
      handleEqual();
    } else {
      setCheckInput(false);
    }

    setResult(number);
    setSign(val);
  };

  const handleDot = () => {
    if (!checkInput) {
      setNumber(0);
      setCheckInput(true);
    }

    for (let i = 0; i < number.length; ++i)
      if (number[i] === '.') return;

    setNumber(number => number + '.')
  };

  const checkLimitDigits = () => {
    let count = 0;
    for (let i = 0; i < number.length; ++i)
      if (!isNaN(number[i])) ++count;
    return count <= 9 && count >= 0;
  };

  const handleInput = (val) => () => {
    if (!checkLimitDigits() && checkInput) return;

    if (!checkInput) {
      setNumber(val);
      setCheckInput(true);
      return;
    }

    let newNumber = number + val;

    if (number === '0') newNumber = val;
    setNumber(newNumber);
  };

  return (
    <div className="Calculator">
      <h1 className="text-center">My Calculator</h1>
      <h3 className="text-center">STANDARD COMPUTER</h3>
      <div id="result">
        <span>{number}</span>
        </div>

        <div id="table">
          <table>
            <tbody>
              <tr>
                <td><button className="btn btn-danger" onClick={reset}>AC</button></td>
                <td><button className="btn btn-danger" onClick={() => setNumber(0)}>CE</button></td>
                <td><button className="btn btn-default" onClick={handlePercent}>%</button></td>
                <td><button className="btn btn-default" onClick={() => handleSign('÷')}>÷</button></td>
              </tr>
              <tr>
                <td><button className="btn btn-success" onClick={handleInput('7')}>7</button></td>
                <td><button className="btn btn-success" onClick={handleInput('8')}>8</button></td>
                <td><button className="btn btn-success" onClick={handleInput('9')}>9</button></td>
                <td><button className="btn btn-default" onClick={() => handleSign('x')}>x</button></td>
              </tr>
              <tr>
                <td><button className="btn btn-success" onClick={handleInput('4')}>4</button></td>
                <td><button className="btn btn-success" onClick={handleInput('5')}>5</button></td>
                <td><button className="btn btn-success" onClick={handleInput('6')}>6</button></td>
                <td><button className="btn btn-default" onClick={() => handleSign('—')}>—</button></td>
              </tr>
              <tr>
                <td><button className="btn btn-success" onClick={handleInput('1')}>1</button></td>
                <td><button className="btn btn-success" onClick={handleInput('2')}>2</button></td>
                <td><button className="btn btn-success" onClick={handleInput('3')}>3</button></td>
                <td rowSpan="2"><button className="btn btn-default" id="plusButton" onClick={() => handleSign('+')}>+</button></td>
              </tr>
              <tr>
                <td><button className="btn btn-default" onClick={handleDot}>.</button></td>
                <td><button className="btn btn-success" onClick={handleInput('0')}>0</button></td>
                <td><button className="btn btn-default" onClick={handleEqual}>=</button></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
}

